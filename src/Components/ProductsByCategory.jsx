import axios from "axios";
import React, { useState, useEffect, useRef } from "react";

import { useQuery } from "react-query";

import "./Products.css";

function fetchProductsByCategory(type) {
  return axios.get(`https://fakestoreapi.com/products/${type}`);
}

function ProductsByCategory() {
  const [inputValue, setInputValue] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const inputRef = useRef(null);
  const categories = [
    "electronics",
    "jewelery",
    "men's clothing",
    "women's clothing",
  ];

  const { data, isLoading, isError, error } = useQuery(
    `category : ${searchValue}`,
    () => fetchProductsByCategory(searchValue)
  );

  function handleSearch() {
    if (categories.includes(inputValue)) {
      setSearchValue("category/" + inputValue);
      setInputValue("");
    }
  }

  useEffect(() => {
    inputRef.current.focus();
  }, [searchValue]);

  return (
    <div className="container-fluid products-container">
      <div className="row">
        <div className="col">
          <h3>Products by Category</h3>
          <input
            className="m-2"
            type="text"
            onChange={(e) => setInputValue(e.target.value)}
            ref={inputRef}
            value={inputValue}
          ></input>
          <button className="btn btn-primary" onClick={handleSearch}>
            Search
          </button>
          <p>We have the following categories : {categories.join(" / ")}</p>
          <p>Selected Category : {searchValue ? searchValue.slice(9) : 'all'}</p>
        </div>
        <div className="row d-flex flex-wrap">
          {isLoading ? (
            <p>Loading products...</p>
          ) : isError ? (
            <p>{error.message}</p>
          ) : (
            data.data.map((eachProduct) => {
              return (
                <div
                  key={eachProduct.id}
                  className="col-10 col-md-3 col-lg-2 mx-3 shadow p-3 mb-5 bg-white rounded text flex-grow-1"
                >
                  <h3 className="product-name">{eachProduct.title}</h3>
                  <img
                    src={eachProduct.image}
                    alt=""
                    className="product-image mb-2 w-100"
                  />
                  <p className="pt-2 text-left">
                    Category :{" "}
                    <span className="product-price">
                      {eachProduct.category}
                    </span>
                  </p>
                  <p className="pt-2 line-clamp">
                    Description :{" "}
                    <span className="text-secondary">
                      {eachProduct.description}
                    </span>
                  </p>
                  <p className="pt-2">
                    Price :{" "}
                    <span className="product-price">{eachProduct.price}</span>
                  </p>
                  <p className="pt-2">
                    Rating :{" "}
                    <span className="product-price">
                      {eachProduct.rating.rate}
                    </span>
                  </p>
                </div>
              );
            })
          )}
        </div>
      </div>
    </div>
  );
}

export default ProductsByCategory;
