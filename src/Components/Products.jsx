import "./Home.css";
import "./Products.css";
import Loader from "./Loader/Loader";


function Products({isLoading, isError, data, error}) {

  if (isLoading) {
    return <Loader />;
  }

  if (isError) {
    return <p className="p-5 m-5">{error.message}</p>;
  }

  return (
    <section className="container-fluid products-container">
      <div className="row">
        {data.data.map((eachProduct) => {
          return (
            <div
              key={eachProduct.id}
              className="col-10 col-md-3 col-lg-2 mx-3 shadow p-3 mb-5 bg-white rounded text flex-grow-1"
            >
              <h3 className="product-name">{eachProduct.title}</h3>
              <img
                src={eachProduct.image}
                alt=""
                className="product-image mb-2 w-100"
              />
              <p className="pt-2 text-left">
                Category :{" "}
                <span className="product-price">{eachProduct.category}</span>
              </p>
              <p className="pt-2 line-clamp">
                Description :{" "}
                <span className="text-secondary">
                  {eachProduct.description}
                </span>
              </p>
              <p className="pt-2">
                Price :{" "}
                <span className="product-price">{eachProduct.price}</span>
              </p>
              <p className="pt-2">
                Rating :{" "}
                <span className="product-price">{eachProduct.rating.rate}</span>
              </p>
            </div>
          );
        })}
      </div>
    </section>
  );
}
export default Products;
