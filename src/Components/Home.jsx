import React from "react";
import { Link } from "react-router-dom";

import "./Home.css";

function Home() {
  return (
    <div className="cointainer-fluid">
      <div className="row m-0">
        <div className="col-12 mx-0 px-5 home-container">
          <h1>
            Welcome to our E-Commerce website. We are exploring a lot products
          </h1>
          <Link to="/products">
            <button className="btn btn-primary">Go to Products</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Home;
