import React, { useReducer } from "react";

import "./Products.css";

const initialCount = {
  countValue: 0,
};

const reducer = (count, action) => {
  switch (action.type) {
    case "increment":
      return { countValue: count.countValue + 1 };
    case "decrement":
      return { countValue: count.countValue - 1 };
    case "reset":
      return initialCount;
    default:
      return count;
  }
};

function CounterWithUseReducer() {
  console.log("useReducer");
  const [count, dispatch] = useReducer(reducer, initialCount);
  return (
    <div className="container-fluid products-container">
      <hr></hr>
      Example of useReducer
      <div className="row">
        <div className="col">
          <div>Count : {count.countValue} </div>
          <button
            className="btn btn-primary m-2"
            onClick={() => dispatch({ type: "increment" })}
          >
            Increment
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => dispatch({ type: "decrement" })}
          >
            Decrement
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => dispatch({ type: "reset" })}
          >
            Reset
          </button>
        </div>
      </div>
      <hr></hr>
    </div>
  );
}

export default CounterWithUseReducer;
