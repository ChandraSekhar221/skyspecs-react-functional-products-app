import React, { useState } from "react";

function Counter() {
  const [count, setCount] = useState(0);

  const handleIncrease = () => setCount(count + 1)

  const handleDecrease = () => setCount(count -1 )
  return (
    <div>
      <p>Your Count : {count}</p>
      <button onClick={handleIncrease}>Increase</button>
      <br></br>
      <button onClick={handleDecrease}>Decrease</button>
    </div>
  );
}

export default Counter;
