import React from "react";

import { Link } from "react-router-dom";

function Header() {
  return (
    <nav className="navbar navbar-expand-md navbar-light bg-light fixed-top">
      <p className="m-0 px-3">Products</p>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div
        className="collapse navbar-collapse justify-content-center"
        id="navbarNav"
      >
        <ul className="navbar-nav">
          <li className="nav-item active align-self-center">
            <Link to="/" className="nav-link active">
              Home
            </Link>
          </li>
          <li className="nav-item active align-self-center">
            <Link to="/products" className="nav-link active">
              Products
            </Link>
          </li>
          <li className="nav-item active align-self-center">
            <Link to="/productsByCategory" className="nav-link active">
              Products by Category
            </Link>
          </li>
          <li className="nav-item active align-self-center">
            <Link to="/examples" className="nav-link active">
              Examples
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Header;
