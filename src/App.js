import './App.css';

import React from 'react';

import { useQuery } from 'react-query'

import axios from 'axios';

import { Switch, Route } from 'react-router-dom'

import Home from './Components/Home';
import Products from './Components/Products';
import Header from './Components/Header';
import ProductsByCategory from './Components/ProductsByCategory';
import CounterWithUseReducer from './Components/CounterWithUseReducer';

const fetchProducts = () => {
  return axios.get("https://fakestoreapi.com/products");
};

function App() {

  const { isLoading, data, isError, error } = useQuery(
    "all-products",
    fetchProducts
  );

  return (
    <>
      <Header />
      <Switch >
        <Route exact path='/' component={Home} />
        <Route exact path='/products' component={() => <Products
          isLoading={isLoading}
          isError={isError}
          data={data}
          error={error}
        />} />
        <Route exact path='/productsByCategory' component={ProductsByCategory} />
        <Route exact path='/examples' component={CounterWithUseReducer} />
      </Switch>
    </>
  );
}

export default App;
